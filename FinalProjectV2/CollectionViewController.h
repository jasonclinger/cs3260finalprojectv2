//
//  CollectionViewController.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/29/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewController : UICollectionViewController
{
    NSMutableArray* arrayOfMetals;
    NSData* data;
    UICollectionView* cView;
    id d;
}

@end
