//
//  Contributor.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 4/2/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contributor : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *imageUrl;

@end
