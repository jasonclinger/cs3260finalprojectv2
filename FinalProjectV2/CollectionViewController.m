//
//  CollectionViewController.m
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/29/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "CollectionViewController.h"
#import "CollectionViewCell.h"
#import "DetailViewController.h"


@interface CollectionViewController (){
    
    
}

@end

@implementation CollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
        NSData *JSONData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://api.myjson.com/bins/4j6pc"]];
    
        NSMutableArray *jsonResult = [NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:nil];
    
        data = JSONData;
        arrayOfMetals = jsonResult;
    
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    d = arrayOfMetals[indexPath.row];
    
    cell.metalName = d[@"name"];
    cell.currentSpotPrice = d[@""];
    cell.roundName = d[@"roundName"];
    cell.roundBuyPrice = d[@""];
    cell.roundSellPrice = d[@""];
    cell.eagleName = d[@"eagleName"];
    
    cell.eagleBuyPrice = d[@""];
    cell.eagleSellPrice = d[@""];
    
    cell.mapleName = d[@"mapleName"];
    cell.mapleBuyPrice = d[@""];
    cell.mapleSellPrice = d[@""];
    cell.junkName = d[@"junkName"];
    cell.junkSilverBuyPrice = d[@""];
    cell.junkSilverSellPrice = d[@""];
    
    cell.textLabel.text = cell.metalName;
    cell.imageView.image = [self imageFromURLString:d[@"image_thumb"]];
    cell.image_thumb = cell.imageView.image;
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier  isEqual: @"detailview"]) {
        CollectionViewCell *cell = sender;
        DetailViewController *detailview = segue.destinationViewController;
        
        detailview->preName = cell.metalName;
        //detailview->preSpotPrice = cell.currentSpotPrice;
        detailview->preRoundName = cell.roundName;
        detailview->preRoundBuy = cell.roundBuyPrice;
        detailview->preRoundSell = cell.roundSellPrice;
        detailview->preEagleName = cell.eagleName;
        //detailview->preEagleBuy = cell.eagleBuyPrice;
        //detailview->preEagleSell = cell.eagleSellPrice;
        detailview->preMapleName = cell.mapleName;
        detailview->preMapleBuy = cell.mapleBuyPrice;
        detailview->preMapleSell = cell.mapleSellPrice;
        detailview->preJunkName = cell.junkName;
        detailview->preJunkBuy = cell.junkSilverBuyPrice;
        detailview->preJunkSell = cell.junkSilverSellPrice;
        detailview->preImage = cell.image_thumb;
    }
    
}

- (UIImage *)imageFromURLString:(NSString *)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"GET"];
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *result = [NSURLConnection sendSynchronousRequest:request
                                           returningResponse:&response error:&error];
    
    UIImage* myImage = [UIImage imageWithData:
                        [NSData dataWithContentsOfURL:
                         [NSURL URLWithString: urlString]]];
    
    
    NSLog(@"urlString: %@",urlString);
    return myImage;
}



@end
