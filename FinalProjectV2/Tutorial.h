//
//  Tutorial.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 4/2/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tutorial : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *url;

@end
