//
//  DetailViewController.m
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/31/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import "DetailViewController.h"

#import "TFHpple.h"
#import "Tutorial.h"
#import "Contributor.h"

@interface DetailViewController (){
    NSMutableArray *_objectsMetals;
    NSMutableArray *_objectsSpot;
  
}

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    array = [NSArray arrayWithObjects: @"Past 7 Days", @"Past Month", @"Past 3 Months", @"Past 6 Months", @"Past Year", nil];
    
    //UIPickerView * picker = [UIPickerView new];
    _myPickerView.delegate = self;
    _myPickerView.dataSource = self;
    _myPickerView.showsSelectionIndicator = YES;
    [self.view addSubview:_myPickerView];
    
    [self loadTutorialsSpot];
    
    [self loadTutorialsMetals];
    
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    //_wonderImage.image = preImage;
    ///////////////////////_spotBuy.text = preSpotPrice;
    _metalName.text = preName;
    _roundName.text = preRoundName;
    _eagleName.text = preEagleName;
    _mapleName.text = preMapleName;
    _junkName.text = preJunkName;
    
    Tutorial *thisTutorialSpot = [_objectsSpot objectAtIndex:1];
    _spotBuy.text = thisTutorialSpot.title;
    
    if ([preName isEqualToString:@"Gold"]) {
        Tutorial *thisTutorialEagleBuy = [_objectsMetals objectAtIndex:0];
        Tutorial *thisTutorialEagleSell = [_objectsMetals objectAtIndex:1];
        
        Tutorial *thisTutorialRoundBuy = [_objectsMetals objectAtIndex:2];
        Tutorial *thisTutorialRoundSell = [_objectsMetals objectAtIndex:3];
        
        Tutorial *thisTutorialJunkBuy = [_objectsMetals objectAtIndex:4];
        Tutorial *thisTutorialJunkSell = [_objectsMetals objectAtIndex:5];
        
        Tutorial *thisTutorialMapleBuy = [_objectsMetals objectAtIndex:6];
        Tutorial *thisTutorialMapleSell = [_objectsMetals objectAtIndex:7];
        
        _roundBuy.text = thisTutorialRoundBuy.title;
        _roundSell.text = thisTutorialRoundSell.title;
        
        _eagleBuy.text = thisTutorialEagleBuy.title;
        _eagleSell.text = thisTutorialEagleSell.title;
        
        _mapleBuy.text = thisTutorialMapleBuy.title;
        _mapleSell.text = thisTutorialMapleSell.title;
        
        _junkBuy.text = thisTutorialJunkBuy.title;
        _junkSell.text = thisTutorialJunkSell.title;
        
    }
    else if ([preName isEqualToString:@"Silver"]){
        Tutorial *thisTutorialEagleBuy = [_objectsMetals objectAtIndex:4];
        Tutorial *thisTutorialEagleSell = [_objectsMetals objectAtIndex:5];
        
        Tutorial *thisTutorialRoundBuy = [_objectsMetals objectAtIndex:2];
        Tutorial *thisTutorialRoundSell = [_objectsMetals objectAtIndex:3];
        
        Tutorial *thisTutorialJunkBuy = [_objectsMetals objectAtIndex:0];
        Tutorial *thisTutorialJunkSell = [_objectsMetals objectAtIndex:1];
        
        Tutorial *thisTutorialMapleBuy = [_objectsMetals objectAtIndex:6];
        Tutorial *thisTutorialMapleSell = [_objectsMetals objectAtIndex:7];
        
        _roundBuy.text = thisTutorialRoundBuy.title;
        _roundSell.text = thisTutorialRoundSell.title;
        
        _eagleBuy.text = thisTutorialEagleBuy.title;
        _eagleSell.text = thisTutorialEagleSell.title;
        
        _mapleBuy.text = thisTutorialMapleBuy.title;
        _mapleSell.text = thisTutorialMapleSell.title;
        
        _junkBuy.text = thisTutorialJunkBuy.title;
        _junkSell.text = thisTutorialJunkSell.title;
        
    }
    else if ([preName isEqualToString:@"Platinum"]){
        Tutorial *thisTutorialMapleBuy = [_objectsMetals objectAtIndex:0];
        Tutorial *thisTutorialMapleSell = [_objectsMetals objectAtIndex:1];
        
        _roundBuy.text = preRoundBuy;
        _roundSell.text = preRoundSell;
        
        _eagleBuy.text = preEagleBuy;
        _eagleSell.text = preEagleSell;
        
        _mapleBuy.text = thisTutorialMapleBuy.title;
        _mapleSell.text = thisTutorialMapleSell.title;
        
        _junkBuy.text = preJunkBuy;
        _junkSell.text = preJunkSell;

    }
    else{ //Palladium
        Tutorial *thisTutorialEagleBuy = [_objectsMetals objectAtIndex:0];
        Tutorial *thisTutorialEagleSell = [_objectsMetals objectAtIndex:1];
        
        Tutorial *thisTutorialMapleBuy = [_objectsMetals objectAtIndex:2];
        Tutorial *thisTutorialMapleSell = [_objectsMetals objectAtIndex:3];
        
        _roundBuy.text = preRoundBuy;
        _roundSell.text = preRoundSell;
        
        _eagleBuy.text = thisTutorialEagleBuy.title;
        _eagleSell.text = thisTutorialEagleSell.title;
        
        _mapleBuy.text = thisTutorialMapleBuy.title;
        _mapleSell.text = thisTutorialMapleSell.title;

        _junkBuy.text = preJunkBuy;
        _junkSell.text = preJunkSell;
    }
    
    
    
    
    
    
    
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return array.count;
}


- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    switch(row) {
        case 0:
            title = array[0];
            break;
        case 1:
            title = array[1];
            break;
        case 2:
            title = array[2];
            break;
        case 3:
            title = array[3];
            break;
        case 4:
            title = array[4];
            break;
    }
    return title;
    
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    //Here, like the table view you can get the each section of each row if you've multiple sections
    NSLog(@"Selected Timeframe: %@. Index of selected time: %li", [array objectAtIndex:row], (long)row);
    
    //Now, if you want to navigate then;
    // Say, OtherViewController is the controller, where you want to navigate:
    
    //OtherViewController *objOtherViewController = [OtherViewController new];
    //[self.navigationController pushViewController:objOtherViewController animated:YES];
    
    
    //other option stay with Display Graph button option to push to new view controller
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)loadTutorialsMetals {
    
    NSURL *tutorialsUrl = [NSURL URLWithString:@"http://www.rustcoin.com/metal_prices.php"];
    NSData *tutorialsHtmlData = [NSData dataWithContentsOfURL:tutorialsUrl];
    
    TFHpple *tutorialsParser = [TFHpple hppleWithHTMLData:tutorialsHtmlData];
    
    NSString *tutorialsXpathQueryString;
    
    if ([preName isEqualToString:@"Gold"]) {
        tutorialsXpathQueryString = @"//div[@class='featured-spot-products']/div[1]/table";
    }
    else if ([preName isEqualToString:@"Silver"]){
        tutorialsXpathQueryString = @"//div[@class='featured-spot-products']/div[2]/table";
    }
    else if ([preName isEqualToString:@"Platinum"]){
        tutorialsXpathQueryString = @"//div[@class='featured-spot-products']/div[3]/table";
    }
    else{ //Palladium
        tutorialsXpathQueryString = @"//div[@class='featured-spot-products']/div[4]/table";
    }
    
    NSArray *tutorialsNodes = [tutorialsParser searchWithXPathQuery:tutorialsXpathQueryString];
    
    NSMutableArray *newTutorials = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (TFHppleElement *element in tutorialsNodes) {
        
        for (TFHppleElement *child in element.children) {
            if ([child.tagName isEqualToString:@"tr"]) {
                
                for(TFHppleElement *grandChild in child.children)
                    if ([grandChild.tagName isEqualToString:@"td"]) {
                        
                        for(TFHppleElement *greatGrandChild in grandChild.children)
                            if ([greatGrandChild.tagName isEqualToString:@"span"]) {
                                @try {
                                    
                                    Tutorial *tutorial = [[Tutorial alloc] init];
                                    [newTutorials addObject:tutorial];
                                    
                                    
                                    tutorial.title = [[greatGrandChild firstChild] content];
                                    //tutorial.url = [greatGrandChild objectForKey:@"nodeContent"];
                                    NSLog(@"found a price");
                                    
                                }
                                @catch (NSException *e) {}
                                
                            }
                        
                    }
                
            } else if ([child.tagName isEqualToString:@"class"]) {
                
            }
        }
    }
    
    _objectsMetals = newTutorials;
    //[self reloadData];
}

-(void)loadTutorialsSpot {
    
    NSURL *tutorialsUrl = [NSURL URLWithString:@"http://www.rustcoin.com/metal_prices.php"];
    NSData *tutorialsHtmlData = [NSData dataWithContentsOfURL:tutorialsUrl];
    
    TFHpple *tutorialsParser = [TFHpple hppleWithHTMLData:tutorialsHtmlData];
    
    NSString *tutorialsXpathQueryString;
    
    if ([preName isEqualToString:@"Gold"]) {
        tutorialsXpathQueryString = @"//div[@class='charts']/div[1]/div[1]";
    }
    else if ([preName isEqualToString:@"Silver"]){
        tutorialsXpathQueryString = @"//div[@class='charts']/div[2]/div[1]";
    }
    else if ([preName isEqualToString:@"Platinum"]){
        tutorialsXpathQueryString = @"//div[@class='charts']/div[3]/div[1]";
    }
    else{ //Palladium
        tutorialsXpathQueryString = @"//div[@class='charts']/div[4]/div[1]";
    }
    
    
    
    //Gold
    //NSString *tutorialsXpathQueryString = @"//div[@class='charts']/div[1]/div[1]";
    
    //Silver
    //NSString *tutorialsXpathQueryString = @"//div[@class='charts']/div[2]/div[1]";
    
    //Platinum
    //NSString *tutorialsXpathQueryString = @"//div[@class='charts']/div[3]/div[1]";
    
    //Palladium
    //NSString *tutorialsXpathQueryString = @"//div[@class='charts']/div[4]/div[1]";
    
    NSArray *tutorialsNodes = [tutorialsParser searchWithXPathQuery:tutorialsXpathQueryString];
    
    NSMutableArray *newTutorials = [[NSMutableArray alloc] initWithCapacity:0];
    
    for (TFHppleElement *element in tutorialsNodes) {
        
        for (TFHppleElement *child in element.children) {
            if ([child.tagName isEqualToString:@"table"]) {
                
                for(TFHppleElement *grandChild in child.children)
                    if ([grandChild.tagName isEqualToString:@"tr"]) {
                        
                        for(TFHppleElement *greatGrandChild in grandChild.children)
                            if ([greatGrandChild.tagName isEqualToString:@"td"]) {
                                @try {
                                    
                                    Tutorial *tutorial = [[Tutorial alloc] init];
                                    [newTutorials addObject:tutorial];
                                    
                                    
                                    tutorial.title = [[greatGrandChild firstChild] content];
                                    //tutorial.url = [greatGrandChild objectForKey:@"nodeContent"];
                                    NSLog(@"found a price");
                                    
                                }
                                @catch (NSException *e) {}
                                
                            }
                        
                    }
                
            } else if ([child.tagName isEqualToString:@"class"]) {
                
            }
        }
    }
    
    _objectsSpot = newTutorials;
    //[self.collectionView reloadData];
}


@end
