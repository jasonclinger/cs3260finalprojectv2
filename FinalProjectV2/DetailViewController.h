//
//  DetailViewController.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/31/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSArray* array;
    NSString* title;
    
    @public NSString* preName;
    //NSString* preSpotPrice;
    
    NSString* preRoundName;
    NSString* preRoundBuy;
    NSString* preRoundSell;
    
    NSString* preEagleName;
    NSString* preEagleBuy;
    NSString* preEagleSell;
    
    NSString* preMapleName;
    NSString* preMapleBuy;
    NSString* preMapleSell;
    
    NSString* preJunkName;
    NSString* preJunkBuy;
    NSString* preJunkSell;
    UIImage* preImage;
    
}

@property (weak, nonatomic) IBOutlet UILabel *metalName;
@property (weak, nonatomic) IBOutlet UILabel *spotBuy;
@property (weak, nonatomic) IBOutlet UILabel *mapleName;
@property (weak, nonatomic) IBOutlet UILabel *mapleBuy;
@property (weak, nonatomic) IBOutlet UILabel *mapleSell;
@property (weak, nonatomic) IBOutlet UILabel *eagleName;
@property (weak, nonatomic) IBOutlet UILabel *eagleBuy;
@property (weak, nonatomic) IBOutlet UILabel *eagleSell;
@property (weak, nonatomic) IBOutlet UILabel *roundName;
@property (weak, nonatomic) IBOutlet UILabel *roundBuy;
@property (weak, nonatomic) IBOutlet UILabel *roundSell;
@property (weak, nonatomic) IBOutlet UILabel *junkName;
@property (weak, nonatomic) IBOutlet UILabel *junkBuy;
@property (weak, nonatomic) IBOutlet UILabel *junkSell;

@property (weak, nonatomic) IBOutlet UIPickerView *myPickerView;







@end
