//
//  CollectionViewCell.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/31/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell

//@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) UIImage* image_thumb;
@property (nonatomic, strong) NSString* metalName;
@property (nonatomic, strong) NSString* currentSpotPrice;
@property (nonatomic, strong) NSString* roundName;
@property (nonatomic, strong) NSString* roundBuyPrice;
@property (nonatomic, strong) NSString* roundSellPrice;
@property (nonatomic, strong) NSString* eagleName;
@property (nonatomic, strong) NSString* eagleBuyPrice;
@property (nonatomic, strong) NSString* eagleSellPrice;
@property (nonatomic, strong) NSString* mapleName;
@property (nonatomic, strong) NSString* mapleBuyPrice;
@property (nonatomic, strong) NSString* mapleSellPrice;
@property (nonatomic, strong) NSString* junkName;
@property (nonatomic, strong) NSString* junkSilverBuyPrice;
@property (nonatomic, strong) NSString* junkSilverSellPrice;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UILabel *textLabel;


@end
