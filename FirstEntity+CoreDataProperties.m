//
//  FirstEntity+CoreDataProperties.m
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/29/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FirstEntity+CoreDataProperties.h"

@implementation FirstEntity (CoreDataProperties)

@dynamic nameEntity;
@dynamic spotPriceEntity;
@dynamic mapleBuyEntity;
@dynamic eagleBuyEntity;
@dynamic mapleSellEntity;
@dynamic eagleSellEntity;
@dynamic roundSellEntity;
@dynamic roundBuyEntity;
@dynamic junkBuyEntity;
@dynamic junkSellEntity;

@end
