//
//  FirstEntity.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/29/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface FirstEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "FirstEntity+CoreDataProperties.h"
