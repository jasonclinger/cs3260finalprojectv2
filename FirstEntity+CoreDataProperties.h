//
//  FirstEntity+CoreDataProperties.h
//  FinalProjectV2
//
//  Created by Jason Clinger on 3/29/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FirstEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface FirstEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *nameEntity;
@property (nullable, nonatomic, retain) NSNumber *spotPriceEntity;
@property (nullable, nonatomic, retain) NSNumber *mapleBuyEntity;
@property (nullable, nonatomic, retain) NSNumber *eagleBuyEntity;
@property (nullable, nonatomic, retain) NSNumber *mapleSellEntity;
@property (nullable, nonatomic, retain) NSNumber *eagleSellEntity;
@property (nullable, nonatomic, retain) NSNumber *roundSellEntity;
@property (nullable, nonatomic, retain) NSNumber *roundBuyEntity;
@property (nullable, nonatomic, retain) NSNumber *junkBuyEntity;
@property (nullable, nonatomic, retain) NSNumber *junkSellEntity;

@end

NS_ASSUME_NONNULL_END
